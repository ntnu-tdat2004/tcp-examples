import net from 'net';

const client = new net.Socket();
client.connect(3000, '127.0.0.1', () => {
  console.log('Client: connected');

  client.write('hello\r\n', (error) => {
    if (!error) console.log('Client: sent: hello');
  });
});

client.on('data', (data) => {
  const message = data.toString().substring(0, data.length - 2);
  console.log('Client: received:', message);

  client.write('exit\r\n', (error) => {
    if (!error) console.log('Client: sent: exit');
  });
});

client.on('end', () => {
  console.log('Client: server closed connection');
});

client.on('error', (error) => {
  console.log('Client: error:', error);
});
