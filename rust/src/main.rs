use std::str;
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::net::TcpListener;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let listener = TcpListener::bind("127.0.0.1:3000").await?;

    println!("Server: waiting for connection");
    loop {
        let (mut socket, _) = listener.accept().await?;
        println!("Server: connection from {}", socket.peer_addr()?);

        tokio::spawn(async move {
            let mut buf = [0; 1024];

            loop {
                let n = match socket.read(&mut buf).await {
                    Ok(0) => {
                        println!("Server: client closed connection");
                        return;
                    }
                    Ok(n) => n,
                    Err(e) => {
                        eprintln!("Server: error: {:?}", e);
                        return;
                    }
                };

                let message = match str::from_utf8(&buf[0..n - 2]) {
                    Ok(v) => v,
                    Err(e) => panic!("Invalid UTF-8 sequence: {}", e),
                };
                println!("Server: received: {}", message);

                if message == "exit" {
                    println!("Server: closing connection");
                    return;
                }

                if let Err(e) = socket.write_all(&buf[0..n]).await {
                    eprintln!("Server: error: {:?}", e);
                    return;
                }
                println!("Server: sent: {}", message);
            }
        });
    }
}
