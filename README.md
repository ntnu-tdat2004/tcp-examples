# Example TCP servers in different programming languages

## Prerequisites
* Linux or MacOS
* The C++ IDE [juCi++](https://gitlab.com/cppit/jucipp) is recommended

## Installing dependencies
The C++ examples make use of the following library:

- A C++ library for network and low-level I/O programming:
  [Asio C++ Library](https://think-async.com/Asio/)

### Manjaro

Run the following command to install this package in Manjaro:

```sh
sudo pacman -S asio
```

### MacOS

Run the following command to install this package in MacOS:

```sh
brew install asio
```
